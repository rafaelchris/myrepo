// describe a suite
describe('My First Test', () => {
  // define the test case
  it('Visits the Kitchen', () => {
    // visit EcoCommons website
    cy.visit('https://ecocommons.org.au')

    // click on 'Our Team' url
    cy.get('[id="menu-item-1168"]')
      .contains('Our team')
      .click({force:true});

    // assert: new page loaded
    cy.url()
      .should('contains', '/about/team/')

    // type in search bar
    cy.get('input[type="search"]')
      .type('NASA')

    // assert: value typed
    cy.get('input[type="search"]')
      .should('have.value', 'NASA')

    // submit search
    cy.get('button[type="submit"]')
      .click()

    // assert: parameter searched
    cy.url().should('contains', 'NASA')

    // click on first result
    cy.get('[class="entry-title"]')
      .first()
      .click()

    // assert article searched
    cy.url()
      .should('contains', 'nasa')

    // select first post from related articles
    cy.get('#rpwwt-recent-posts-widget-with-thumbnails-2')
      .find('ul')
      .find('li')
      .first()
      .click()

    cy.get('[id="menu-item-1784"]')
    .contains('Sign in')
    .click({force:true});

    cy.get('.Header-module_navLinks__1mb8Z')
    .find('li')
    .eq(0)
    .click()

  })
})
